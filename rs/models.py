from django.db import models

# Create your models here.

class Regi(models.Model):
	
	password=models.CharField(max_length=20)
	email=models.CharField(max_length=30)

	def __str__(self):
		return u'%s %s %s'% (self.email,self.password,self.id)
