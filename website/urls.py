from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from rs import views



urlpatterns = [
    # Examples:
    # url(r'^$', 'website.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
	
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', views.user, name ='user'),
    url(r'^updated/$', views.updated, name='updated'),
  	url(r'^log/$', views.log, name='log'),
  
    

]
# if settings.DEBUG:
# 	urlpatterns +=static(settings.STATIC_URL,document_root=settings.staticroot)
#  	urlpatterns +=static(settings.media_URL,document_root=settings.media_root)